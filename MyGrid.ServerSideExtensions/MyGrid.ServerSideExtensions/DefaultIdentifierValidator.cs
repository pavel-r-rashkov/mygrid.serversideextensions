﻿using MyGrid.ServerSideExtensions.Contracts;
using System;
using System.Text.RegularExpressions;

namespace MyGrid.ServerSideExtensions
{
    public class DefaultIdentifierValidator : IIdentifierValidator
    {
        public bool ValidateIdentifier(string identifier)
        {
            if (String.IsNullOrWhiteSpace(identifier))
            {
                throw new ArgumentException("Identifier cannot be null or white space");
            }

            return Regex.IsMatch(identifier, "^[\\w]+$");
        }
    }
}
