﻿namespace MyGrid.ServerSideExtensions.ColumnFilters
{
    public class FilterOptions
    {
        public string FilterKey { get; set; }

        public string PropertyName { get; set; }

        public object FilterData { get; set; }
    }
}
