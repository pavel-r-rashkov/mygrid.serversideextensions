﻿using MyGrid.ServerSideExtensions.Contracts;
using System;

namespace MyGrid.ServerSideExtensions.ColumnFilters
{
    class FilterFactory
    {
        private const string FilterClassSufix = "Filter";
        private const string Namespace = "MyGrid.ServerSideExtensions.ColumnFilters";

        public static IColumnFilter CreateFromKey(string filterKey)
        {
            if (String.IsNullOrWhiteSpace(filterKey))
            {
                throw new ArgumentException("filterKey cannot be null or white space");
            }

            var filterName = filterKey + FilterClassSufix;
            var filterInstance = Activator.CreateInstance(Type.GetType(Namespace + "." + filterName, true, true)) as IColumnFilter;
            if (filterInstance == null)
            {
                throw new ArgumentException("Cannot find filter with name " + filterName);
            }

            return filterInstance;
        }
    }
}
