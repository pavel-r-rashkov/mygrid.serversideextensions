﻿using MyGrid.ServerSideExtensions.Contracts;
using MyGrid.ServerSideExtensions.Enums;
using System;
using System.Linq;
using System.Linq.Dynamic;

namespace MyGrid.ServerSideExtensions.ColumnFilters
{
    class DateFilter : IColumnFilter
    {
        public IQueryable<T> Filter<T>(IQueryable<T> query, string propertyName, dynamic filterData)
        {
            DateTime date = Convert.ToDateTime(filterData.selectedDate);
            DateSearchTermType searchTermType = (DateSearchTermType)filterData.searchTermType;

            string whereClauseOperator = null;
            switch (searchTermType)
            {
                case DateSearchTermType.LessThan:
                    whereClauseOperator = "<";
                    break;
                case DateSearchTermType.LessThanOrEqual:
                    whereClauseOperator = "<=";
                    break;
                case DateSearchTermType.Equals:
                    whereClauseOperator = "=";
                    break;
                case DateSearchTermType.MoreThanOrEqual:
                    whereClauseOperator = ">=";
                    break;
                case DateSearchTermType.MoreThan:
                    whereClauseOperator = ">";
                    break;
                default:
                    throw new ArgumentException("Unrecognized search term type " + searchTermType);
            }

            return query.Where(propertyName + " " + whereClauseOperator + " DateTime(@0, @1, @2)", date.Year, date.Month, date.Day);
        }
    }
}
