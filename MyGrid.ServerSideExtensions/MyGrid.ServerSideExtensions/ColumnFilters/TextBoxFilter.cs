﻿using MyGrid.ServerSideExtensions.Contracts;
using MyGrid.ServerSideExtensions.Enums;
using System;
using System.Linq;
using System.Linq.Dynamic;

namespace MyGrid.ServerSideExtensions.ColumnFilters
{
    class TextBoxFilter : IColumnFilter
    {
        public IQueryable<T> Filter<T>(IQueryable<T> query, string propertyName, dynamic filterData)
        {
            string text = Convert.ToString(filterData.text);
            TextSearchTermType searchTermType = (TextSearchTermType)filterData.searchTermType;
            return query.Where(propertyName + "." + searchTermType.ToString() + "(@0)", text);
        }
    }
}
