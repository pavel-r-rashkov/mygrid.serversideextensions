﻿using MyGrid.ServerSideExtensions.Contracts;
using MyGrid.ServerSideExtensions.Enums;
using System;
using System.Linq;
using System.Linq.Dynamic;

namespace MyGrid.ServerSideExtensions.ColumnFilters
{
    class SpinFilter : IColumnFilter
    {
        public IQueryable<T> Filter<T>(IQueryable<T> query, string propertyName, dynamic filterData)
        {
            double number = Convert.ToDouble(filterData.number);
            SpinSearchTermType searchTermType = (SpinSearchTermType)filterData.searchTermType;

            string whereClauseOperator = null;
            switch (searchTermType)
            {
                case SpinSearchTermType.LessThan:
                    whereClauseOperator = "<";
                    break;
                case SpinSearchTermType.LessThanOrEqual:
                    whereClauseOperator = "<=";
                    break;
                case SpinSearchTermType.Equals:
                    whereClauseOperator = "=";
                    break;
                case SpinSearchTermType.MoreThanOrEqual:
                    whereClauseOperator = ">=";
                    break;
                case SpinSearchTermType.MoreThan:
                    whereClauseOperator = ">";
                    break;
                default:
                    throw new ArgumentException("Unrecognized search term type " + searchTermType);
            }

            return query.Where(propertyName + " " + whereClauseOperator + " @0", number);
        }
    }
}
