﻿using MyGrid.ServerSideExtensions.Contracts;
using System;
using System.Linq;
using System.Linq.Dynamic;

namespace MyGrid.ServerSideExtensions.ColumnFilters
{
    class MarkFilter : IColumnFilter
    {
        public IQueryable<T> Filter<T>(IQueryable<T> query, string propertyName, dynamic filterData)
        {
            bool isMarked = Convert.ToBoolean(filterData.isMarked);
            return query.Where(propertyName + " = @0", isMarked);
        }
    }
}
