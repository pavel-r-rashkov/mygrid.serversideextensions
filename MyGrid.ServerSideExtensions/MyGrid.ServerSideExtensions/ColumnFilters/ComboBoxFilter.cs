﻿using MyGrid.ServerSideExtensions.Contracts;
using System;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;

namespace MyGrid.ServerSideExtensions.ColumnFilters
{
    class ComboBoxFilter : IColumnFilter
    {
        public IQueryable<T> Filter<T>(IQueryable<T> query, string propertyName, dynamic filterData)
        {
            Type propertyType = typeof(T).GetProperty(propertyName).PropertyType;
            if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                propertyType = Nullable.GetUnderlyingType(propertyType);
            }

            object comboBoxValue = null;

            if (propertyType == typeof(Guid))
            {
                comboBoxValue = Guid.Parse(Convert.ToString(filterData.comboBoxValue));
            }
            else if (propertyType == typeof(double))
            {
                comboBoxValue = double.Parse(Convert.ToString(filterData.comboBoxValue), CultureInfo.InvariantCulture);
            }
            else if (propertyType == typeof(decimal))
            {
                comboBoxValue = decimal.Parse(Convert.ToString(filterData.comboBoxValue), CultureInfo.InvariantCulture);
            }
            else if (propertyType == typeof(int))
            {
                comboBoxValue = int.Parse(Convert.ToString(filterData.comboBoxValue));
            }
            else if (propertyType == typeof(bool))
            {
                comboBoxValue = bool.Parse(Convert.ToString(filterData.comboBoxValue));
            }
            else
            {
                comboBoxValue = Convert.ToString(filterData.comboBoxValue);
            }

            return query.Where(propertyName + " = @0", comboBoxValue);
        }
    }
}
