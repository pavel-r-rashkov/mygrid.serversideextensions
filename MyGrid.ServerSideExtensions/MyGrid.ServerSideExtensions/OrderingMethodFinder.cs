﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace MyGrid.ServerSideExtensions
{
    class OrderingMethodFinder : ExpressionVisitor
    {
        public OrderingMethodFinder()
        {
            this.MethodFound = false;
        }

        public bool MethodFound { get; set; }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            var name = node.Method.Name;

            if (node.Method.DeclaringType == typeof(Queryable) && (
                name.StartsWith("OrderBy", StringComparison.Ordinal) ||
                name.StartsWith("ThenBy", StringComparison.Ordinal)))
            {
                this.MethodFound = true;
            }

            return base.VisitMethodCall(node);
        }
    }
}
