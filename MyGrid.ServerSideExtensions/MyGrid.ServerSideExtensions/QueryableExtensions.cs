﻿using MyGrid.ServerSideExtensions.ColumnFilters;
using MyGrid.ServerSideExtensions.Contracts;
using MyGrid.ServerSideExtensions.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Reflection;
using System.Text;

namespace MyGrid.ServerSideExtensions
{
    public static class QueryableExtensions
    {
        public static GridResult ApplyGridState<T>(this IQueryable<T> query, GridState gridState, IIdentifierValidator identifierValidator = null)
        {
            if (gridState == null)
            {
                throw new ArgumentException("GridState cannot be null");
            }

            if (identifierValidator == null)
            {
                identifierValidator = new DefaultIdentifierValidator();
            }

            if (gridState.FilterState != null)
            {
                query = query.ApplyFilters(gridState.FilterState);
            }

            if (gridState.OrderState != null)
            {
                query = query.ApplyOrdering(gridState.OrderState);
            }

            IQueryable<object> filteredAndOrdered = (IQueryable<object>)query;
            if (gridState.GroupingState != null)
            {
                filteredAndOrdered = filteredAndOrdered.ApplyGrouping(gridState.GroupingState);
            }

            IEnumerable<SummaryResult> summaryResults = null;
            if (gridState.SummaryStates != null)
            {
                summaryResults = filteredAndOrdered.CalculateSummary(gridState.SummaryStates);
            }

            int resultsCount = 0;
            if (gridState.PaginationState != null)
            {
                resultsCount = filteredAndOrdered.Count();
                filteredAndOrdered = filteredAndOrdered.ApplyPagination(gridState.PaginationState);
            }

            var items = filteredAndOrdered.ToList();
            if (gridState.PaginationState == null)
            {
                resultsCount = items.Count;
            }

            var gridResult = new GridResult()
            {
                Items = items,
                Count = resultsCount,
                SummaryResults = summaryResults
            };

            return gridResult;
        }

        public static IQueryable<T> ApplyFilters<T>(this IQueryable<T> query, FilterState state, IIdentifierValidator identifierValidator = null)
        {
            if (state == null)
            {
                throw new ArgumentException("FilterState cannot be null");
            }

            if (identifierValidator == null)
            {
                identifierValidator = new DefaultIdentifierValidator();
            }

            FilterOptions invalidFilterOption = state.ColumnFilters.Values.FirstOrDefault(f => !identifierValidator.ValidateIdentifier(f.PropertyName));
            if (invalidFilterOption != null)
            {
                throw new InvalidIdentifierException(invalidFilterOption.PropertyName);
            }

            foreach (var columnFilter in state.ColumnFilters)
            {
                IColumnFilter filter = FilterFactory.CreateFromKey(columnFilter.Value.FilterKey);
                query = filter.Filter(query, columnFilter.Value.PropertyName, columnFilter.Value.FilterData);
            }

            return query;
        }

        public static IQueryable<T> ApplyOrdering<T>(this IQueryable<T> query, OrderState state, IIdentifierValidator identifierValidator = null)
        {
            if (state == null)
            {
                throw new ArgumentException("OrderState cannot be null");
            }

            if (String.IsNullOrWhiteSpace(state.PropertyName))
            {
                throw new ArgumentException("Cannot order by null or white space PropertyName");
            }

            if (identifierValidator == null)
            {
                identifierValidator = new DefaultIdentifierValidator();
            }

            if (!identifierValidator.ValidateIdentifier(state.PropertyName))
            {
                throw new InvalidIdentifierException(state.PropertyName);
            }

            string orderByClause = state.PropertyName + " " + (state.IsAscending ? "ascending" : "descending");
            return query.OrderBy(orderByClause);
        }

        public static IQueryable<object> ApplyGrouping(this IQueryable<object> query, GroupingState state, IIdentifierValidator identifierValidator = null)
        {
            if (state == null)
            {
                throw new ArgumentException("GroupingState cannot be null");
            }

            if (String.IsNullOrWhiteSpace(state.PropertyName))
            {
                throw new ArgumentException("Cannot group by null or white space PropertyName");
            }

            if (identifierValidator == null)
            {
                identifierValidator = new DefaultIdentifierValidator();
            }

            if (!identifierValidator.ValidateIdentifier(state.PropertyName))
            {
                throw new InvalidIdentifierException(state.PropertyName);
            }

            var orderByClause = state.PropertyName + " " + (state.IsAscending ? "ascending" : "descending");

            return (query
                .GroupBy(state.PropertyName, "it")
                .Select("new (it.Key as " + state.PropertyName + ", it as groupItems)") as IQueryable<object>)
                .OrderBy(orderByClause);
        }

        public static IQueryable<T> ApplyPagination<T>(this IQueryable<T> query, PaginationState state)
        {
            if (state == null)
            {
                throw new ArgumentException("PaginationState cannot be null");
            }

            var orderingFinder = new OrderingMethodFinder();
            orderingFinder.Visit(query.Expression);

            if (!orderingFinder.MethodFound)
            {
                query = query.OrderBy(t => 1);
            }

            return query
                .Skip((state.CurrentPage - 1) * state.PageSize)
                .Take(state.PageSize);
        }

        public static IEnumerable<SummaryResult> CalculateSummary<T>(this IQueryable<T> query, IEnumerable<SummaryState> states, IIdentifierValidator identifierValidator = null)
        {
            if (states == null)
            {
                throw new ArgumentException("Summary states cannot be null");
            }

            if (states.Count() <= 0)
            {
                return new List<SummaryResult>();
            }

            if (identifierValidator == null)
            {
                identifierValidator = new DefaultIdentifierValidator();
            }
            
            IEnumerable<SummaryState> summaryStates = states.Distinct();
            var selectClauseSegments = new List<string>();

            foreach (var summaryState in summaryStates)
            {
                if (String.IsNullOrWhiteSpace(summaryState.SummaryType))
                {
                    throw new ArgumentException("Summary type cannot be null or white space");
                }

                if (String.IsNullOrWhiteSpace(summaryState.PropertyName))
                {
                    throw new ArgumentException("Summary state property name cannot be null or white space");
                }

                if (!identifierValidator.ValidateIdentifier(summaryState.PropertyName))
                {
                    throw new InvalidIdentifierException(summaryState.PropertyName);
                }
                
                switch(summaryState.SummaryType.ToLower())
                {
                    case "avg":
                        selectClauseSegments.Add(String.Format("Average(it.{0}) as {1}", summaryState.PropertyName, summaryState.PropertyName + "_" + summaryState.SummaryType));
                        break;
                    case "min":
                        selectClauseSegments.Add(String.Format("Min(it.{0}) as {1}", summaryState.PropertyName, summaryState.PropertyName + "_" + summaryState.SummaryType));
                        break;
                    case "max":
                        selectClauseSegments.Add(String.Format("Max(it.{0}) as {1}", summaryState.PropertyName, summaryState.PropertyName + "_" + summaryState.SummaryType));
                        break;
                    case "count":
                        selectClauseSegments.Add(String.Format("it.Where({0} != null).Count() as {1}", summaryState.PropertyName, summaryState.PropertyName + "_" + summaryState.SummaryType));
                        break;
                    case "sum":
                        selectClauseSegments.Add(String.Format("Sum(it.{0}) as {1}", summaryState.PropertyName, summaryState.PropertyName + "_" + summaryState.SummaryType));
                        break;
                    default:
                        throw new ArgumentException(String.Format("Unrecognized summary type: {0}", summaryState.SummaryType));
                }
            }

            string selectClause = "new(" + String.Join(", ", selectClauseSegments) + ")";
            object summaryValue = (query.GroupBy("1", "it").Select(selectClause) as IQueryable<object>).FirstOrDefault();

            var summaryResults = new List<SummaryResult>();
            foreach (var summaryState in summaryStates)
            {
                var summaryResult = new SummaryResult()
                {
                    SummaryType = summaryState.SummaryType,
                    PropertyName = summaryState.PropertyName,
                };

                string key = summaryState.PropertyName + "_" + summaryState.SummaryType;
                Type type = summaryValue.GetType();
                PropertyInfo propertyInfo = type.GetProperty(key);
                summaryResult.SummaryValue = propertyInfo.GetValue(summaryValue);
                summaryResults.Add(summaryResult);
            }

            return summaryResults;
        }
    }
}
