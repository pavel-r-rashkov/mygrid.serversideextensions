﻿namespace MyGrid.ServerSideExtensions.States
{
    public class PaginationState
    {
        public int CurrentPage { get; set; }

        public int PageSize { get; set; }
    }
}
