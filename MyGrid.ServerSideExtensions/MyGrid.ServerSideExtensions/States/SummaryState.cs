﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGrid.ServerSideExtensions.States
{
    public class SummaryState : IEquatable<SummaryState>, IComparable<SummaryState>
    {
        public string  PropertyName { get; set; }

        public string SummaryType { get; set; }

        public int CompareTo(SummaryState other)
        {
            if (other == null)
            {
                return -1;
            }

            if (this.PropertyName == other.PropertyName 
                && this.SummaryType.ToLower() == other.SummaryType.ToLower())
            {
                return 0;
            }

            return (this.PropertyName + this.SummaryType).CompareTo(other.PropertyName + other.SummaryType);
        }

        public bool Equals(SummaryState other)
        {
            if (other == null)
            {
                return false;
            }

            return this.PropertyName == other.PropertyName
                && this.SummaryType.ToLower() == other.SummaryType.ToLower();
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash = 7 * hash + this.PropertyName.GetHashCode();
            hash = 7 * hash + this.SummaryType.GetHashCode();
            return hash;
        }
    }
}
