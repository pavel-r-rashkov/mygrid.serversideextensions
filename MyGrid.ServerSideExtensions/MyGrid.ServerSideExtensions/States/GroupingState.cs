﻿namespace MyGrid.ServerSideExtensions.States
{
    public class GroupingState
    {
        public string PropertyName { get; set; }

        public bool IsAscending { get; set; }
    }
}
