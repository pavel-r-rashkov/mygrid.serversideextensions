﻿using System.Collections.Generic;

namespace MyGrid.ServerSideExtensions.States
{
    public class GridState
    {
        public FilterState FilterState { get; set; }

        public GroupingState GroupingState { get; set; }

        public OrderState OrderState { get; set; }

        public PaginationState PaginationState { get; set; }

        public IEnumerable<SummaryState> SummaryStates { get; set; }
    }
}
