﻿using MyGrid.ServerSideExtensions.ColumnFilters;
using System.Collections.Generic;

namespace MyGrid.ServerSideExtensions.States
{
    public class FilterState
    {
        public Dictionary<string, FilterOptions> ColumnFilters { get; set; }
    }
}
