﻿namespace MyGrid.ServerSideExtensions.States
{
    public class OrderState
    {
        public string PropertyName { get; set; }

        public bool IsAscending { get; set; }
    }
}
