﻿namespace MyGrid.ServerSideExtensions.Enums
{
    enum DateSearchTermType
    {
        LessThan,
        LessThanOrEqual,
        Equals,
        MoreThanOrEqual,
        MoreThan
    }
}
