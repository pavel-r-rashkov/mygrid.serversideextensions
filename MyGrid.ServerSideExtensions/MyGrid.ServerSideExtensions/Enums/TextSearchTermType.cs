﻿namespace MyGrid.ServerSideExtensions.Enums
{
    enum TextSearchTermType
    {
        StartsWith,
        Contains,
        EndsWith
    }
}
