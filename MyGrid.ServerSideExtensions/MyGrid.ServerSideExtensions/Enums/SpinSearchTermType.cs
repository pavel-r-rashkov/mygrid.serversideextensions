﻿namespace MyGrid.ServerSideExtensions.Enums
{
    enum SpinSearchTermType
    {
        LessThan,
        LessThanOrEqual,
        Equals,
        MoreThanOrEqual,
        MoreThan
    }
}
