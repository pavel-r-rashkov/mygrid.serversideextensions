﻿using System.Collections.Generic;

namespace MyGrid.ServerSideExtensions
{
    public class GridResult
    {
        public IEnumerable<object> Items { get; set; }

        public int Count { get; set; }

        public IEnumerable<SummaryResult> SummaryResults { get; set; }
    }
}
