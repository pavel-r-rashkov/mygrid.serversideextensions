﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGrid.ServerSideExtensions
{
    public class SummaryResult
    {
        public string PropertyName { get; set; }

        public string SummaryType { get; set; }

        public object SummaryValue { get; set; }
    }
}
