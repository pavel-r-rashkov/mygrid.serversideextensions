﻿namespace MyGrid.ServerSideExtensions.Contracts
{
    public interface IIdentifierValidator
    {
        bool ValidateIdentifier(string identifier);
    }
}
