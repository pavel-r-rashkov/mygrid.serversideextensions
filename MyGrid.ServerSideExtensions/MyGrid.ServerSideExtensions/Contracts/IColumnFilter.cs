﻿using System.Linq;

namespace MyGrid.ServerSideExtensions.Contracts
{
    public interface IColumnFilter
    {
        IQueryable<T> Filter<T>(IQueryable<T> query, string propertyName, object filterData);
    }
}
