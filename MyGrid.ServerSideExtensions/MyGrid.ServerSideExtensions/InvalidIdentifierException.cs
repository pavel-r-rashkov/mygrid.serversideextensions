﻿using System;

namespace MyGrid.ServerSideExtensions
{
    class InvalidIdentifierException : Exception
    {
        public InvalidIdentifierException(string propertyName)
            : base(String.Format("Invalid PropertyName: {0}. Implement IIdentifierValidator if you want to use custom validation logic", propertyName))
        {
            this.InvalidPropertyName = propertyName;
        }

        public string InvalidPropertyName { get; private set; }
    }
}
