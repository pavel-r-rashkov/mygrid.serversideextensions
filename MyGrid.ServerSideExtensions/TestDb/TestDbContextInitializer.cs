﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDb
{
    class TestDbContextInitializer : DropCreateDatabaseAlways<TestDbContext>
    {
        public TestDbContextInitializer()
        {
        }

        protected override void Seed(TestDbContext context)
        {
            base.Seed(context);
        }
    }
}
