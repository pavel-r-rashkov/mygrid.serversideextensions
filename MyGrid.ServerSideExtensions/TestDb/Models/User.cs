﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDb.Models
{
    public class User
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int? Age { get; set; }

        public DateTime? BirthDate { get; set; }

        public int? UserType { get; set; }

        public bool? IsActive { get; set; }

        public bool? BoolTest { get; set; }

        public Guid? GuidTest { get; set; }

        public char? CharTest { get; set; }

        public decimal? DecimalTest { get; set; }
    }
}
