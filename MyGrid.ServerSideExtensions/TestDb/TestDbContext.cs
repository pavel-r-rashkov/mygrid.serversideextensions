﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDb.Models;

namespace TestDb
{
    public class TestDbContext : DbContext
    {
        public TestDbContext()
            : base("name=TestDbContext")
        {
            Database.SetInitializer(new TestDbContextInitializer());
        }

        public virtual DbSet<User> Users { get; set; }
    }
}
