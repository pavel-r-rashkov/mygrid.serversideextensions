# MyGrid.ServerSideExtensions #
This repo contains .NET extension methods for server-side processing for [MyGrid](https://bitbucket.org/pavel-r-rashkov/mygrid)
[NuGet package](https://www.nuget.org/packages/MyGrid.ServerSideExtensions)